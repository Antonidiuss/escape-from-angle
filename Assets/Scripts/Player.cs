﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameScr gameScr;

    public int ID = 0;

    float movemenet = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        movemenet = Input.GetAxisRaw("Jump");
    }

    private void FixedUpdate()
    {
        transform.RotateAround(Vector3.zero, Vector3.forward, Time.deltaTime * gameScr.moveSpeed);

        if (movemenet == 0)
        {
            movemenet = -1;
        }

        if (transform.position.magnitude < gameScr.minRadius)
        {
            if (movemenet < 0)
            {
                movemenet = 0;

            }
        }
        else if (transform.position.magnitude > gameScr.maxRadius)
        {
            if (movemenet > 0)
            {
                movemenet = 0;

            }
        }


        transform.position = transform.position + transform.position * 0.04f * movemenet;
    }

}
