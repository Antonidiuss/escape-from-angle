﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject : MonoBehaviour
{
    public float moveSpeed = 0.01f;
    public CrushObject _crushObj;


    // Start is called before the first frame update
    void Start()
    {
       GetComponent<SpriteRenderer>().sprite = _crushObj._sprite;
    }

    // Update is called once per frame
    void Update()
    {
           
    }


    private void FixedUpdate()
    {
        transform.position = transform.position - transform.position * Time.deltaTime * moveSpeed;

        if (transform.position.magnitude < 1.5f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _crushObj.onEnter(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
