﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScr : MonoBehaviour
{
    public int core = 0;
    private int highscore = 0;
    public float minRadius, maxRadius;
    public float maxMoveSpeed = 150f;
    
    public float MoveSlow = 120f;

    public GameObject PlayerPrefab;
    public Text text, maxText;

    public float SpawnRate = 5f;

    [HideInInspector()]
    public float moveSpeed;

    public bool isGameStarted = false;

    private float timeInSlow, timeToSlow;

    public List<GameObject> Players = new List<GameObject>();




    public void FixedUpdate()
    {
        if (timeToSlow != 0)
        {
            CheckSlowTime();
        }
    }

    void ScoreUpdate()
    {
        text.text = core.ToString();
        if (core > highscore)
        {
            highscore = core;
            maxText.text = highscore.ToString();
        }
    }


    public void StartGame()
    {
        moveSpeed = maxMoveSpeed;
        AddPlayer();
        AddPlayer();
        AddPlayer();
        isGameStarted = true;
    }


    void CheckSlowTime()
    {
        if (timeInSlow >= timeToSlow)
        {
            moveSpeed = maxMoveSpeed;
            timeToSlow = 0;
            timeInSlow = 0;
        }
        else
        {
            timeInSlow += Time.deltaTime;
        }
    }


    public void AddPlayer()
    {
        float angle = Random.Range(0, Mathf.PI * 2);
        Vector2 pos2d = new Vector2(Mathf.Sin(angle) * (maxRadius+minRadius)/2, Mathf.Cos(angle) * (maxRadius + minRadius) / 2);

        GameObject obj = Instantiate(PlayerPrefab, pos2d, Quaternion.identity);
        obj.GetComponent<Player>().gameScr = this;
        Players.Add(obj);
        core++;
        ScoreUpdate();
    }

    public void removePlayer(GameObject obj)
    {
        foreach (GameObject ob in Players)
        {
            if (obj == ob)
            {
                Players.Remove(ob);
                Destroy(obj);
                core--;
                ScoreUpdate();
                CheckLoose();
                break;
            }
        }
    }

    public void SlowTime()
    {
        moveSpeed = moveSpeed-MoveSlow;
        timeToSlow = 3f;
    }


    public void CheckLoose()
    {
        if (Players.Count == 0)
        {
            //################################################################# LOSE GAME
        }
    }
}
