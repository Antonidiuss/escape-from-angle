﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameScr gameScr;

    public GameObject objPrefab;

    public Sprite Proton, Barrier, SlowTime;

    private float nextTimeToSpawn = 0f;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameScr.isGameStarted)
        {
            CheckSpawn();
        }
    }

    void CheckSpawn()
    {
        if (Time.time >= nextTimeToSpawn)
        {
            float angle = Random.Range(0, Mathf.PI * 2);
            Vector2 pos2d = new Vector2(Mathf.Sin(angle) * (gameScr.maxRadius + 5f), Mathf.Cos(angle) * (gameScr.maxRadius+5f));

            GameObject GO = Instantiate(objPrefab, pos2d, Quaternion.identity);
            int rnd = Random.Range(1, 6);
            if (rnd < 3)
            {
                GO.GetComponent<MapObject>()._crushObj = BuildCrash("Barrier", Barrier, new BarrierModel());
            }
            else if (rnd < 5)
            {
                GO.GetComponent<MapObject>()._crushObj = BuildCrash("Proton", Proton, new AddProtonModel());
            }
            else
            {
                GO.GetComponent<MapObject>()._crushObj = BuildCrash("Slow Time", SlowTime, new SlowTimeModel());
            }

            nextTimeToSpawn = Time.time + 1f / gameScr.SpawnRate;
        }
    }

    CrushObject BuildCrash(string name, Sprite sprite, CrashModel crashModel)
    {
        return new CrushObject(name, sprite, gameScr, crashModel);
    }
}
