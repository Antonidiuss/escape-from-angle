﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfCrush
{

}

public class CrushObject : ScriptableObject
{
    public string _name;
    public Sprite _sprite;
    public CrashModel _model;

    public void onEnter(GameObject gameObject)
    {
        _model.OnEnter(gameObject);
    }

    public CrushObject(string name, Sprite sprite, GameScr gameScr, CrashModel model)
    {
        _name = name;
        _sprite = sprite;
        model.gameScr = gameScr;
        _model = model;

    }
}





public abstract class CrashModel
{

    public GameScr gameScr;
    public abstract void OnEnter(GameObject gameObject);
    
}


public class AddProtonModel : CrashModel
{
    public override void OnEnter(GameObject gameObject)
    {
        gameScr.AddPlayer();
    }

}

public class BarrierModel : CrashModel
{

    public override void OnEnter(GameObject gameObject)
    {
        gameScr.removePlayer(gameObject);
    }
}

public class SlowTimeModel : CrashModel
{
    public override void OnEnter(GameObject gameObject)
    {
        gameScr.SlowTime();
    }
}